
export class Profil {
    constructor(public login:string="?",
                public name:string="?",
                public followers
                :string="?",
                public following
                :string="?",
                public location:string="?",
                public public_repos:string="?",
                public bio : string ="?",
                public blog : string ="?",
                public email : string ="?"
               
                ){}
}