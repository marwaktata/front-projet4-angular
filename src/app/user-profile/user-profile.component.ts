import { Component, OnInit, Injectable, Inject } from '@angular/core';
import { ServiceUser } from 'app/service/serviceUser';
import { userInfo } from 'os';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {


  cheminImage: string = "";
  public imageProfil() {
    let idUser = Number(sessionStorage.getItem("userId"));
    if (idUser == 187) {
      this.cheminImage = "./assets/img/faces/marwa.jpg"
    }
    else this.cheminImage = "./assets/img/faces/marc.jpg"
    console.log("chemin image " + this.cheminImage)
  }
  firstName = "";
  lastName = "";
  email = "";
  country = "";
  city = "";
  adress = "";
  PostalCode = "";
  username = "";
  firstNameAp = "";
  lastNameAp = "";
  emailAp = "";
  countryAp = "";
  cityAp = "";
  adressAp = "";
  PostalCodeAp = "";
  usernameAp = "";
  constructor(private serviceUser: ServiceUser, public dialog: MatDialog) { }

  public getUser() {
    let idUser = Number(sessionStorage.getItem("userId"));
    this.serviceUser.getLogedUser(idUser).subscribe((user) => {
      // let   user = users[0];
      this.firstName = user.nom;
      this.lastName = user.prenom;
      this.email = user.email;
      this.country = user.pays;
      this.city = user.ville;
      this.PostalCode = user.codePostale;
      this.username = user.username;
      this.adress = user.adresse;
      console.log("test " + this.firstName)
    })

  }
 /*  public updateUser() {
   let  idUser = Number(sessionStorage.getItem("userId"));
    
    this.serviceUser.updateUser(idUser, this.username, this.lastName).subscribe(
     
      (user) => {
      this.lastName = user.nom;

    })
  } */
  ngOnInit() {
    this.imageProfil();
    this.getUser();
  }

  openDialog(): void {
    let idUser = Number(sessionStorage.getItem("userId"));
    const dialogRef = this.dialog.open(DialogEditUserComponent, {
      width: '750px',
      data: { username: this.username, id: idUser, lastName : this.lastName },
    })


    console.log('The dialog was opened');

    dialogRef.afterClosed().subscribe(result => {
      this.getUser();
      console.log("The dialog was closed and result is " + result);
      result  = this.lastName
       console.log("result is "+result)
//this.updateUser();
      // this.ajoutProfil();
      // this.getProfil();
    });
  }
}

@Component({
  selector: 'app-dialog-edit-user',
  templateUrl: './dialog-edit-user.component.html',
  styleUrls: ['./dialog-edit-user.component.css']
})
export class DialogEditUserComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogEditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserProfileComponent) { }

  onNoClick(): void {
    this.dialogRef.close();
  }



  ngOnInit(): void {
  }

}