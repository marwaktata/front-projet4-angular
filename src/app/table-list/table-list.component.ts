import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ServiceProfil } from '../service/serviceProfil';
import { ServiceFollowings } from '../service/serviceFollowings'
import { Profil } from '../model/profil';
import { LoginComponent } from '../login/login.component';

import { UsernameResponse } from 'app/model/usernameResponse';
import { FollowingsResponse } from 'app/model/followingsResponse';




@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
@Injectable({
  providedIn: 'root'
})
export class TableListComponent implements OnInit {
  username: string;
  test: string = "";
  message: string;
  public profils: Profil[];
  onSupprimerProfil(idProfil) {
    this.serviceProfil.deleteProfil(idProfil)
      .subscribe(
        () => { this.message = "ok"; },
        (error) => {
          console.log(error);
          this.message = JSON.stringify(error);
        }
      );


  }
  onSupprimerUsername(username) {
    this.serviceProfil.deleteUsername(username)
      .subscribe(
        () => { this.message = "ok"; },
        (error) => {
          console.log(error);
          this.message = JSON.stringify(error);
        }
      );


  }
  onSupprimerFollowingBYIdProfil(idProfil) {
    this.serviceFollowings.deleteFollowingByIdProfil(idProfil)
      .subscribe(
        () => { this.message = "ok"; },
        (error) => {
          console.log(error);
          this.message = JSON.stringify(error);
        }
      );
  
  
  }

  deleteRow(d) {
    const index = this.profils.indexOf(d);
    this.profils.splice(index, 1);
    this.onSupprimerFollowingBYIdProfil(d.id);
    this.onSupprimerProfil(d._id);
    console.log(d._id)
    this.onSupprimerUsername(d.login);
  
  }



  constructor(private serviceProfil: ServiceProfil, public dialog: MatDialog, 
    public serviceFollowings: ServiceFollowings ) { }

  getProfil() {
    this.serviceProfil.getAllProfiles().subscribe(
      profils => {
        this.profils = profils
        // console.log(JSON.stringify(this.profils))
        sessionStorage.setItem("profilsRepo",profils[0].public_repos);
      }
    )


  }
  ajoutUsername() {

    {
      this.serviceProfil.addUsername(this.username).subscribe((usernameResponse: UsernameResponse) => {
        console.log("username from usernameRespons " + JSON.stringify(usernameResponse));


      },
        (err) => { console.log(err); });

      console.log("username " + JSON.stringify(this.username));
    }
  }
  
  
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: { username: this.username, _id: "" },
    })


    console.log('The dialog was opened');

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed and result is " + result);
      this.username = result;

      this.ajoutUsername();
     
    });
    
  }
  refresh(){
    window.location.reload();
  }

  ngOnInit() {
    this.getProfil();

  }
  addFollowings(d) {
    console.log("name "+ sessionStorage.getItem("userId"))
    this.serviceFollowings.addFollowings(d.login, Number(sessionStorage.getItem("userId")), d.name,d.bio,d.location, d.public_repos, d.id, d.blog,d.email).subscribe((followingsResponse: FollowingsResponse) => {
      console.log("followinfResponse from folowingResponse " + JSON.stringify(followingsResponse));
      //if(d.login==followingsResponse.username){
      this.onClick();
    //}
    },
      (err) => { console.log(err); });
//this.onClick();
   
  }
  
  isActive = false;
  onClick() {
    this.isActive = !this.isActive;
    }
  }





@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})

export class DialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TableListComponent) { }

  onNoClick(): void {
    this.dialogRef.close();
  }



  ngOnInit(): void {
  }

}









