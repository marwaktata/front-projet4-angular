 import { async, ComponentFixture, TestBed } from '@angular/core/testing';

//import  {validateEmail}  from './validateNewUser';
const register =  require('../validation/validationUser')

describe("test email",()=>{
 // let  register :RegisterComponent;
  it ("should return true",()=>{
    expect(register.validateEmail("marwaktata15@gmail.com")).toBeTruthy()
  })
  it ("should return false",()=>{
    expect(register.validateEmail("marwaktata")).toBeFalsy()
  })
  it ("should return false",()=>{
    expect(register.validateEmail("")).toBeFalsy()
  })
})
describe("test password",()=>{

   it ("should return false",()=>{
     expect(register.CheckPassword("")).toBeFalsy()
   })
 })
 describe("test empty",()=>{

  it ("should return true",()=>{
    expect(register.checkEmpty("")).toBeTruthy()
  })
  it ("should return false",()=>{
    expect(register.checkEmpty("test")).toBeFalsy()
  })
})