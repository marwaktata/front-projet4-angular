import { Component, OnInit } from '@angular/core';
import { ServiceUser } from '../service/serviceUser';
import { Router } from '@angular/router';
import { User } from 'app/model/user';
const register =  require('../validation/validationUser')
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
user : User = new User();
name ="";
lastName =""
username ="";
city ="";
password =""
adress="";
postalCode="";
country="";
email ="";
message ="";
messageP ="";
messageEmpty ="";
validateEmail(email){
  if (register.validateEmail(email)){
    this.message ="message : your email is valid"
  }else{
    this.message =  " message : your email is invalid"
  }}
  validateMp(password){
    if (register.CheckPassword(password)){
      this.messageP ="message : your password is valid "
    }else{
      this.messageP =  " message : your password should contain (1 lowercase, 1 uppercase, 1 number)"
    }}
  constructor(private serviceUser : ServiceUser, private router : Router ) { }
 addUser(){
 this.user.adresse =  this.adress ;
this.user.codePostale = this.postalCode;
this.user.email =  this.email;
this.user.motdepasse =  this.password;
this.user.username =  this.username;
this.user.ville =  this.city;
this.user.pays =  this.country;
this.user.prenom =  this.lastName;
this.user.nom = this.name

if(register.validateEmail(this.email)&&(register.CheckPassword(this.password))&&!register.checkEmpty(this.adress)&&!register.checkEmpty(this.name)&&!register.checkEmpty(this.lastName)){
   this.serviceUser.addUser(this.user).subscribe(user=> this.user = user)
   this.router.navigate(['/'])
 }else{
  this.validateEmail(this.email)
  this.validateMp(this.password)
  this.messageEmpty = "message : all field should be not empty!"
 }
 }
 
 
  ngOnInit(): void {
  }

} 


