import { Component, OnInit } from '@angular/core';
import { Followings } from 'app/model/followings';
import { ServiceFollowings } from 'app/service/serviceFollowings';

@Component({
  selector: 'app-profiles-followings',
  templateUrl: './profiles-followings.component.html',
  styleUrls: ['./profiles-followings.component.css']
})
export class ProfilesFollowingsComponent implements OnInit {
 followings : Followings[];
  constructor( private serviceFollowings : ServiceFollowings) { }
   nameUser = (sessionStorage.getItem("name")) ;
   lastname =  sessionStorage.getItem("lastname");
public getFollowingUser(){
 let id = Number(sessionStorage.getItem("userId")) ;

  this.serviceFollowings.listFollowingUser(id).subscribe((followings)=>{this.followings=followings
   let i = this.followings.length;
   if (this.followings.length>5){
    sessionStorage.setItem("followingsNumber",(this.followings.length).toString());
  
  sessionStorage.setItem("repo1",this.followings[Number(this.followings.length)-1].public_repos);
  sessionStorage.setItem("repo2",this.followings[Number(this.followings.length)-2].public_repos);
  sessionStorage.setItem("repo3",this.followings[Number(this.followings.length)-3].public_repos);
  sessionStorage.setItem("repo4",this.followings[Number(this.followings.length)-4].public_repos);
  sessionStorage.setItem("repo5",this.followings[Number(this.followings.length)-5].public_repos);
  
  sessionStorage.setItem("Name1",this.followings[Number(this.followings.length)-1].name);
  sessionStorage.setItem("Name2",this.followings[Number(this.followings.length)-2].name);
  sessionStorage.setItem("Name3",this.followings[Number(this.followings.length)-3].name);
  sessionStorage.setItem("Name4",this.followings[Number(this.followings.length)-4].name); 
  sessionStorage.setItem("Name5",this.followings[Number(this.followings.length)-5].name); 

  sessionStorage.setItem("UserName1",this.followings[Number(this.followings.length)-1].username);
  sessionStorage.setItem("UserName2",this.followings[Number(this.followings.length)-2].username);
  sessionStorage.setItem("UserName3",this.followings[Number(this.followings.length)-3].username);
  sessionStorage.setItem("UserName4",this.followings[Number(this.followings.length)-4].username); 
  sessionStorage.setItem("UserName5",this.followings[Number(this.followings.length)-5].username);
  
  sessionStorage.setItem("location1",this.followings[Number(this.followings.length)-1].location);
  sessionStorage.setItem("location2",this.followings[Number(this.followings.length)-2].location);
  sessionStorage.setItem("location3",this.followings[Number(this.followings.length)-3].location);
  sessionStorage.setItem("location4",this.followings[Number(this.followings.length)-4].location); 
  sessionStorage.setItem("location5",this.followings[Number(this.followings.length)-5].location);
  console.log(this.followings[Number(this.followings.length-4)].username);
}
})
  
}
message ="";
onSupprimerFollowing(idFollowing) {
  this.serviceFollowings.deleteFollowing(idFollowing)
    .subscribe(
      () => { this.message = "ok"; },
      (error) => {
        console.log(error);
        this.message = JSON.stringify(error);
      }
    );


}
refresh(){
  window.location.reload();
}

deleteRow(d) {
  const index = this.followings.indexOf(d);
  this.followings.splice(index, 1);
  //window.location.reload();
  this.onSupprimerFollowing(d.id);
 
 
}
  ngOnInit(): void {
    this.getFollowingUser();
    
  }

}
