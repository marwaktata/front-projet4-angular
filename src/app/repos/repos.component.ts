import { Component, OnInit } from '@angular/core';
import {ServiceRepo} from '../service/serviceRepo'
import { Repo } from '../model/repo';

@Component({
  selector: 'app-repos',
  templateUrl: './repos.component.html',
  styleUrls: ['./repos.component.css']
})

export class ReposComponent implements OnInit {
  public repo: Repo[]=[];
  username  ="sokra"
  fakeArray = new Array(12);
  constructor(private serviceRepo: ServiceRepo) { }
  
  getRepo() {
    this.serviceRepo.getRepoByUserName(this.username).subscribe(
      (repo )=> {this.repo= repo
       
    console.log(JSON.stringify(repo[0]))
      }
    )
    
  
  }

  ngOnInit(): void {
    this.getRepo()
  }

}
