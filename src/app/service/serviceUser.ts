import { Inject,Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../model/user';

 
@Injectable({
    providedIn: 'root'
    })
export class ServiceUser {
    private _headers = new HttpHeaders({'Content-Type': 'application/json'}); 
 constructor(private http: HttpClient)   {
   
 }
   
  
 public getLogedUser(idUser : Number): Observable<User> {
     
    let baseUrl = "https://best-dev-back-spring.herokuapp.com/user/recherche/"+`${idUser}`;

    let wsUrl = `${baseUrl}`;
    return this.http.get<User>(wsUrl);

  }
 
 // Update User
 updateUser(id : Number,username : string, lastname: string, ): Observable<User> {
   let   url: string  = 'https://best-dev-back-spring.herokuapp.com/user/modifierUtilisateur'
   let body = {username : username,id: id, lastname :  lastname}
    return this.http.put<User>(url, body, {headers: this._headers});
  }
addUser(user:User) : Observable<User>{
let url : string ='https://best-dev-back-spring.herokuapp.com/user/addUser'
return this.http.post<User>(url,user, {headers: this._headers})
}
  
}