import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Contributor } from '../model/contributor';
@Injectable({
    providedIn: 'root'
    })
  export class ServiceContributor {
    constructor(private http: HttpClient) { }
    public getAllContributorByRepo(repo:string): Observable<Contributor[]> {
  
      let baseUrl ="https://best-dev-node-staging.herokuapp.com/contribution/"+`${repo}`;
  
      let wsUrl = `${baseUrl}`;
      return this.http.get<Contributor[]>(wsUrl);
  
    }
  }