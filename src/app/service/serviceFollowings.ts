import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { FollowingsResponse } from '../model/followingsResponse';
import { Followings } from '../model/followings';
@Injectable({
    providedIn: 'root'
    })
export class ServiceFollowings {
    private _headers = new HttpHeaders({'Content-Type': 'application/json'}); 
    constructor(private http: HttpClient) { }
    public addFollowings(username : string, idUser: number, name: string, bio : string, location: string, public_repos : string, idProfil : number, blog : string,email: string): Observable<FollowingsResponse>{
        let  addUrl : string =" https://best-dev-back-spring.herokuapp.com/userFollowing/addFollowing"
        const body = {username: username, idUser : idUser, name: name, bio: bio, location :location, public_repos: public_repos, idProfil : idProfil, blog : blog, email:email} 
        return this.http.post<FollowingsResponse>(addUrl,body, {headers: this._headers})
             
             
       }

       public listFollowingUser(idUser: number) : Observable<Followings[]>{
        let  getUrl : string =" https://best-dev-back-spring.herokuapp.com/userFollowing/rechercheParIdUser/"+idUser
        return this.http.get<Followings[]>  (getUrl);
       }
       public deleteFollowing(followingId):Observable<any>{
        let deleteUrl : string = " https://best-dev-back-spring.herokuapp.com/userFollowing/supprimer/" + followingId ;
        console.log("deleteUrl= " + deleteUrl );
        return this.http.delete(deleteUrl );
        }
        public deleteFollowingByIdProfil(IdProfil):Observable<any>{
            let deleteUrl : string = " https://best-dev-back-spring.herokuapp.com/userFollowing/supprimerFollowingFromProfil/" + IdProfil ;
            console.log("deleteUrl= " + deleteUrl );
            return this.http.delete(deleteUrl );
            }
}