import { Inject,Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../model/user';

 
@Injectable({
    providedIn: 'root'
    })
export class ServiceLogin {
   
 constructor(private http: HttpClient)   {
   
 }
   
 
   
    public getLogedUser(nom : string, motdepasse : string): Observable<User[]> {
     
      let baseUrl = " https://best-dev-back-spring.herokuapp.com/user/rechercheParNomEtMotdepasse/"+`${nom}`+"/"+`${motdepasse}`;
  
      let wsUrl = `${baseUrl}`;
      return this.http.get<User[]>(wsUrl);
  
    }}