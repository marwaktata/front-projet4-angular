import { Inject,Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Repo } from '../model/repo';

 
@Injectable({
    providedIn: 'root'
    })
export class ServiceRepo {
   
 constructor(private http: HttpClient)   {
   
 }
   
 
   
    public getRepoByUserName(username : string): Observable<Repo[]> {
     
      let baseUrl = "https://best-dev-node-staging.herokuapp.com/repos/"+`${username}`;
  
      let wsUrl = `${baseUrl}`;
      return this.http.get<Repo[]>(wsUrl);
  
    }}