import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Profil } from '../model/profil';
import { Username } from '../model/username';
import { UsernameResponse } from '../model/usernameResponse';
import { ProfilResponse } from '../model/profilResponse';


@Injectable({
  providedIn: 'root'
})
export class ServiceProfil {
  private _headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) { }
  public getAllProfiles(): Observable<Profil[]> {

    let baseUrl = "https://best-dev-node-staging.herokuapp.com/profiles";

    let wsUrl = `${baseUrl}`;
    return this.http.get<Profil[]>(wsUrl);

  }

  public deleteProfil(profilId): Observable<any> {
    let deleteUrl: string = "https://best-dev-node-staging.herokuapp.com/profil/delete/" + profilId;
    console.log("deleteUrl= " + deleteUrl);
    return this.http.delete(deleteUrl);
  }
  public deleteUsername(username): Observable<any> {
    let deleteUrl: string = "https://best-dev-node-staging.herokuapp.com/username/delete/" + username;
    console.log("deleteUrl= " + deleteUrl);
    return this.http.delete(deleteUrl);
  }
  public addUsername(username: string): Observable<UsernameResponse> {
    let addUrl: string = "https://best-dev-node-staging.herokuapp.com/username/add"
    const body = { username: username }
    return this.http.post<UsernameResponse>(addUrl, body, { headers: this._headers })


  }


}