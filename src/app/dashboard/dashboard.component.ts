import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { ServiceProfil } from '../service/serviceProfil'
import { Profil } from '../model/profil';
import { profile } from 'console';
import { ServiceFollowings } from 'app/service/serviceFollowings';
import { Followings } from 'app/model/followings';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  repoF = [];
  nameF = [];
  followings : Followings[];
  lastname = (sessionStorage.getItem("lastname"));
  name = (sessionStorage.getItem("name"));
  numberFollowings = '';
  
  repo1 = null
  repo2 = null
  repo3 = null
  repo4 = null
  repo5 = null
  name1 = ''
  name2 = ''
  name3 = ''
  name4 = ''
  name5 = ''
  username1 = '';
  username2 = '';
  username3 = '';
  username4 = '';
  username5 = '';
  city1 = '';
  city2 = '';
  city3 = '';
  city4 = "";
  city5 = "";
  public getFollowingUser(){
    let id = Number(sessionStorage.getItem("userId")) ;
   
     this.serviceFollowings.listFollowingUser(id).subscribe((followings)=>{this.followings=followings
    
  })}
 
async topFollowings(){
  for (let i=0;i<5;i++){
   await  this.getFollowingUser();
    this.nameF[i] = this.followings[i].name;
    this.repoF[i] = this.followings[i].public_repos;
   

  }
}

 
  constructor(private serviceProfil: ServiceProfil, private serviceFollowings : ServiceFollowings) { }
 
  startAnimationForBarChart(chart) {
    let seq2: any, delays2: any, durations2: any;

    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on('draw', function (data) {
      if (data.type === 'bar') {
        seq2++;
        data.element.animate({
          opacity: {
            begin: seq2 * delays2,
            dur: durations2,
            from: 0,
            to: 1,
            easing: 'ease'
          }
        });
      }
    });

    seq2 = 0;
  };
  
  ngOnInit() {
    /* this.topFollowings();
    
    var dataRepoViewsChart = {
      labels: this.nameF,
      series: [
       this.repoF

      ]
    };
 */



this.numberFollowings = sessionStorage.getItem("followingsNumber");
    this.repo1 =Number(sessionStorage.getItem("repo1"))
    this.repo2 = Number(sessionStorage.getItem("repo2"))
    this.repo3 = Number(sessionStorage.getItem("repo3"))
    this.repo4 = Number(sessionStorage.getItem("repo4"))
    this.repo5 = Number(sessionStorage.getItem("repo5"))
    this.name1 = sessionStorage.getItem("Name1")
    this.name2 = sessionStorage.getItem("Name2")
    this.name3 = sessionStorage.getItem("Name3")
    this.name4 = sessionStorage.getItem("Name4")
    this.name5 = sessionStorage.getItem("Name5")

    this.username1 = sessionStorage.getItem("UserName1");
    this.username2 = sessionStorage.getItem("UserName2");
    this.username3 = sessionStorage.getItem("UserName3");
    this.username4 = sessionStorage.getItem("UserName4");
    this.username5 = sessionStorage.getItem("UserName5");


    this.city1 = sessionStorage.getItem("location1");
    this.city2 = sessionStorage.getItem("location2");
    this.city3 = sessionStorage.getItem("location3");
    this.city4 = sessionStorage.getItem("location4");
    this.city5 = sessionStorage.getItem("location5");
    


    var dataRepoViewsChart = {
      labels: [this.name1, this.name2, this.name3, this.name4, this.name5],
      series: [
        [this.repo1, this.repo2, this.repo3, this.repo4, this.repo5]
    
      ]
    };


    

   

   

    

   
  




    var optionswebsiteViewsChart = {
      axisX: {
        showGrid: false
      },
      low: 0,
      high: 400,
      chartPadding: { top: 0, right: 5, bottom: 0, left: 0 }
    };
    var responsiveOptions: any[] = [
      ['screen and (max-width: 640px)', {
        seriesBarDistance: 2,
        axisX: {
          labelInterpolationFnc: function (value) {
            return value[0];
          }
        }
      }]
    ];
    var websiteViewsChart = new Chartist.Bar('#websiteViewsChart', dataRepoViewsChart, optionswebsiteViewsChart, responsiveOptions);

   
    this.startAnimationForBarChart(websiteViewsChart);
  }

}
