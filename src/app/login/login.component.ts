import { Component, OnInit,Injectable } from '@angular/core';
import { ServiceLogin } from '../service/serviceLogin';
import { Router } from '@angular/router';
import { User } from '../model/user';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginComponent]
})
@Injectable({
  providedIn: 'root'
  })
export class LoginComponent implements OnInit {
  constructor(private serviceLogin : ServiceLogin, private router : Router ) {  }
  name ="";
password ="";
message ="";
 onClic(){
  this.router.navigate(['/register'])
}

  getUserLogged()  {
    console.log(this.password)
    this.serviceLogin.getLogedUser(this.name,this.password).subscribe(
      
      (users )=> {
    //  let   user = users[0]
   //   console.log("user name "+users[0].nom+ users[0].codePostale)
        if (this.name ==users[0].nom && (this.password==users[0].motdepasse )){
        
         sessionStorage.setItem("userId", users[0].id.toString());
         sessionStorage.setItem("lastname",users[0].prenom);
         sessionStorage.setItem("name", users[0].nom);
console.log("this is " +  sessionStorage.getItem("userId"))
          this.router.navigate(['/dashboard'])
        
         }else{
           this.message = "please check your name and password"
         }
      }
    )
   
  
  }
  
 test(){
  this.getUserLogged();
 

}

  ngOnInit(): void {
  }

}
